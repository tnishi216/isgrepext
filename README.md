# isgrepext README

## Features

Pick up text around current cursor and search on isGrep.

## Requirements

isGrep extension requires isGrep app for Windows.
isGrep version must be 1.6.2 or later.
See:
http://pdic.la.coocan.jp/isgrep/

## Extension Settings

Command name: isGrep

Default Shortcut key: Ctrl+K Ctrl+E

## Known Issues

None.

## Release Notes

### 0.0.1

Initial release.

### 0.0.2

Remove debug message.

### 0.0.3

Change display name (isGrepExt -> isGrep extension)

### 0.0.5

Include numeric character for search pattern.

-----------------------------------------------------------------------------------------------------------
