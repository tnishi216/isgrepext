// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { exec } from 'child_process';
import * as path from 'path';
import { readFile, readFileSync } from 'fs';

function ischar(str: string): boolean {
	if (str.match(/[_A-Za-z0-9']/)) {
		return true;
	}
	return false;
}

function get_isgrep_path(): string {
	let localAppData = process.env['LOCALAPPDATA'] || "";
	let isgrep_path = path.join(localAppData, "isGrep", 'path');
	return readFileSync(isgrep_path, {encoding: 'utf-8'});
}

function exec_isgrep(str: string){
	let isgrep = get_isgrep_path();
	exec(isgrep+" \""+str+"\"", (error,stdout,stderr) =>
	{
		//実行した後の処理をここに書く。実行したアプリが終了した後に実行される。
	});
}

export function activate(context: vscode.ExtensionContext) {
	let disposable = vscode.commands.registerCommand('extension.isgrep', function () {
		let editor = vscode.window.activeTextEditor; // エディタ取得
		if (!editor){
			return;
		}
	
		let doc = editor.document;            // ドキュメント取得
		let cur_selection = editor.selection; // 選択範囲取得

		if (editor.selection.isEmpty){
			let lineno = editor.selection.start.line;
			let line = doc.lineAt(lineno).text;

			// 選択範囲が空であればカーソルのある単語を取得
			// 単語の先頭を探す
			let start = editor.selection.start.character;
			if (start > 0){
				let c = line.substr(start-1, 1);
				if (ischar(c)){
					start--;
				}
			}
			let c = line.substr(start, 1);
			if (!ischar(c)){
				// 文字が見つかるまで右へ
				while (start < line.length){
					if (ischar(line.substr(start, 1))){
						break;
					}
					start++;
				}
				if (start === line.length){
					return;
				}
			} else {
				// カーソル位置は単語
				// 左側を調べる
				while (true){
					let c = line.substr(start, 1);
					if (ischar(c)){
						if (start > 0){
							start--;
							continue;
						}
						break;
					}
					start++;
					break;
				}
			}

			let end = start;
			while (true){
				if(end === line.length){
					break;
				}
				let c = line.substr(end, 1);
				if (!ischar(c)){
					break;
				}
				end++;
			}
			if (start !== end){
				let text = line.substr(start, end-start);
				exec_isgrep(text);
			}
		} else {
			let text = doc.getText(cur_selection); //取得されたテキスト
			exec_isgrep(text);
		}
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
